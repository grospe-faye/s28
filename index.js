// num 3 - creata a fetch request using GET method that will retrieve all the to do list items form JSON placeholder API
// num 4 - using the data retrieved, create an array using the map method to return just the title of every item and print the result  in the console.
fetch('https://jsonplaceholder.typicode.com/todos/', {
	method: 'GET'
})
.then((response) => response.json())
.then((json) => {
	//console.log(json[0].title)
	let list = [];
	json.map((titleOnly) => {
		list += titleOnly.title
	})
	console.log(list)
})

// num 5 - create a fetch request using the GET method that will retrieve a single to do list item from JSON placeholder API
// num 6 - using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET'
})
.then((response) => response.json())
.then((json) => {
	console.log(json)
	console.log(`The item "${json.title}" on the list has a status of ${json.completed}`)
})
//The item "delectus aut autem" on the list has a status of false

// num 7 - create a fetch request using the POST method that will create a to do list item using the JSON placeholder API
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'Created To Do List Item',
		completed: false,
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// num 8 - create a fetch request using the PUT method that will update a to do list item using the JSON placeholder API
/* num 9 - update a to do list item by changing the data structure to contain the following properties:
	a. title
	b. description
	c. status
	d. date completed
	e. user id
*/
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item',
		description: 'To update my to do list with a different data structure',
		status: 'Pending',
		dateCompleted: 'Pending',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// num 10 - create a fetch request using the PATCH method that will update a to do list item using the JSON Place holder API
// num 11 - update a to do list item by changing the status to complete and add a date when the status was changed
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item',
		description: 'To update my to do list with a different data structure',
		status: 'Complete',
		dateCompleted: '10/25/21',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// For confirmation only
/* num 12 - create a request via postman to retrieve all the to do list items
	a. GET HTTP method
	b. https://jsonplaceholder.typicode.com/todos URI endpoint
	c. save this request as get all to do list items
*/

/* num 14 - create via Postman to retrieve an individual to do list item
	a. GET HTTP method
	b. https://jsonplaceholder.typicode.com/todos/1 URI endpoint
	c. save this request as get to do list items
*/
/* num 15 - create a request cia postman to create a to do list item.
	a. POST HTTP method
	b. https://jsonplaceholder.typicode.com/todos URI endpoint
	c. save this request as create to do list items
*/